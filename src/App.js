import React, { Component } from "react";
import "./App.css";
import Tabs from "./components/Tabs";
import ForecastDisplay from "./components/ForecastDisplay";
import { API, APIKEY, location } from "./static/vars";
import DropDown from "./components/DropDown";
import { weatherStaticDataNationwide } from "./static/weatherDataStatic";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      location: location,
      screenData: null,
      serviceDown: false,
      isOpen: false,
      weatherDataNationwide: [],
      weatherStaticDataNationwide: []
    };
  }

  //get weather data from external API
  getWeatherData = () => {
    // get weather data from API based on UK locations
    // defined in location object
    this.state.location.map(location => {
      fetch(API + location.id + "?apikey=" + APIKEY)
        .then(resp => {
          return resp.json();
        }) // Convert data to json
        .then(data => {
          switch (location.id) {
            case 326966:
              data.Location = "Birmingham";
              this.state.weatherDataNationwide.push(data);
              break;
            case 328328:
              data.Location = "London";
              this.state.weatherDataNationwide.push(data);
              break;
            case 327202:
              data.Location = "Cardiff";
              this.state.weatherDataNationwide.push(data);
              break;

            case 329260:
              data.Location = "Manchester";
              this.state.weatherDataNationwide.push(data);
              break;

            case 298885:
              data.Location = "Newcastle";
              this.state.weatherDataNationwide.push(data);
              break;

            case 330510:
              data.Location = "Liverpool";
              this.state.weatherDataNationwide.push(data);
              break;

            case 328226:
              data.Location = "Glasgow";
              this.state.weatherDataNationwide.push(data);
              break;

            case 327336:
              data.Location = "Edinburgh";
              this.state.weatherDataNationwide.push(data);
              break;
            default:
              data.Location = "";
              console.log("No Location traced... >> ");
          }
        })
        .catch(error => {
          // if API service is down or any network issue use static data
          this.setState({ serviceDown: true });
          this.setState({ weatherDataNationwide: weatherStaticDataNationwide });
        });
      return null;
    });
  };

  //display weather forcast of target location
  // set Title of location
  handleLocationChange = name => {
    //update the screen location name
    this.setState({ title: name });
    //update forcast data
    this.state.weatherDataNationwide.map(obj => {
      if (obj.Location === name) {
        this.setState({ screenData: obj });
      }
      return null;
    });
  };

  // toggle drop down menu
  handleMenuToggle = () => {
    if (this.state.isOpen) {
      this.setState({ isOpen: false });
    } else {
      this.setState({ isOpen: true });
    }
  };

  // close deop down menu
  closeMenu = () => this.setState({ isOpen: false });

  // fetch data from API
  componentDidMount() {
    this.setState({ isLoading: true });
    this.getWeatherData();
  }

  render() {
    return (
      <div className="app-wrapper">
        <DropDown
          handleLocationChange={this.handleLocationChange}
          handleMenuToggle={this.handleMenuToggle}
          isOpen={this.state.isOpen}
        />
        <Tabs
          location={this.state.location}
          handleLocationChange={this.handleLocationChange}
          closeMenu={this.closeMenu}
        />

        <ForecastDisplay
          title={this.state.title}
          screenData={this.state.screenData}
          serviceDown={this.state.serviceDown}
        />
      </div>
    );
  }
}

export default App;
