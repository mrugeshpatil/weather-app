//conver Temperature Fahrenheit to Celcius
export default function TemperatureConverter(temp) {
  return Math.round((temp - 32) / 1.8);
}
