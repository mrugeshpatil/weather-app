import React, { Component } from "react";
class Tabs extends Component {
  renderTabs() {
    return (
      <ul className="tabs">
        {this.props.location.map((obj, index) => {
          if (index < 3) {
            return (
              <li
                className=""
                key={obj.name}
                id={obj.name}
                onClick={event => {
                  this.props.handleLocationChange(obj.name);
                  this.props.closeMenu();
                }}
              >
                {obj.name}
              </li>
            );
          }
          return null;
        })}
      </ul>
    );
  }

  render() {
    return <div>{this.renderTabs()}</div>;
  }
}

export default Tabs;
