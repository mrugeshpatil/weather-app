import React, { Component } from "react";
import { location } from "./../static/vars";
import "../css/drop-down.css";

class DropDown extends Component {
  state = {
    defaultText: "Select Location",
    location: location
  };

  render() {
    return (
      <div className="drop-down">
        <div className="drop-down_header">
          <span>{this.state.defaultText}</span>
          <button
            onClick={() => {
              this.props.handleMenuToggle();
            }}
          >
            &#8693;
          </button>
        </div>

        <ul
          className={
            this.props.isOpen === true
              ? "drop-down_menu"
              : "drop-down_menu hide"
          }
        >
          {this.state.location.map((obj, index) => {
            if (index > 2) {
              return (
                <li
                  key={obj.name}
                  onClick={event => {
                    this.props.handleLocationChange(obj.name);
                    this.props.handleMenuToggle();
                  }}
                >
                  {obj.name}
                </li>
              );
            }
            return null;
          })}
        </ul>
      </div>
    );
  }
}

export default DropDown;
