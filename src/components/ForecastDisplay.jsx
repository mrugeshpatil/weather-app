import React, { Component } from "react";
import DateFormatter from "./utils/DateFormatter";
import TemperatureConverter from "./utils/TemperatureConverter";
class ForecastDisplay extends Component {
  render() {
    let screenData = this.props.screenData;

    //inital screen data don't show any weather unless any location is selected
    if (screenData === null) {
      return (
        <React.Fragment>
          <p>Please select location</p>
          {this.props.serviceDown && (
            <small>
              <i>The weather API service is down this is demo data</i>
            </small>
          )}
        </React.Fragment>
      );
    }

    let { Text } = screenData.Headline;

    return (
      <React.Fragment>
        <div className="forecast-panel">
          <h2 className="forecast-panel_title">
            <small>weather forcast - </small>
            <big>{this.props.title}</big>
          </h2>
          <h5>Headline - {Text}</h5>
          <div>
            {screenData.DailyForecasts.map(day => (
              <ul key={day.Date} className="forcast-panel_tile">
                <li>{DateFormatter(day.Date)}</li>
                <li>
                  Temperature Min:{" "}
                  {TemperatureConverter(day.Temperature.Minimum.Value)}
                  <span className="temp-unit">&#8451;</span>
                </li>
                <li>
                  Temperature Max:{" "}
                  {TemperatureConverter(day.Temperature.Maximum.Value)}
                  <span className="temp-unit">&#8451;</span>
                </li>
                <li>Day: {day.Day.IconPhrase}</li>
                <li>Night: {day.Night.IconPhrase}</li>
                <li></li>
              </ul>
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default ForecastDisplay;
