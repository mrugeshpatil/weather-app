//Birmingham weather static data
const weatherBirminghamStaticData = {
  Location: "Birmingham",
  Headline: {
    EffectiveDate: "2020-01-06T13:00:00+00:00",
    EffectiveEpochDate: 1578315600,
    Severity: 3,
    Text: "Rain Monday afternoon",
    Category: "rain",
    EndDate: "2020-01-06T19:00:00+00:00",
    EndEpochDate: 1578337200,
    MobileLink:
      "http://m.accuweather.com/en/gb/birmingham/b5-5/extended-weather-forecast/326966?lang=en-us",
    Link:
      "http://www.accuweather.com/en/gb/birmingham/b5-5/daily-weather-forecast/326966?lang=en-us"
  },
  DailyForecasts: [
    {
      Date: "2020-01-06T07:00:00+00:00",
      EpochDate: 1578294000,
      Temperature: {
        Minimum: {
          Value: 40,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 47,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 35,
        IconPhrase: "Partly cloudy",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/birmingham/b5-5/daily-weather-forecast/326966?day=1&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/birmingham/b5-5/daily-weather-forecast/326966?day=1&lang=en-us"
    },
    {
      Date: "2020-01-07T07:00:00+00:00",
      EpochDate: 1578380400,
      Temperature: {
        Minimum: {
          Value: 47,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 54,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/birmingham/b5-5/daily-weather-forecast/326966?day=2&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/birmingham/b5-5/daily-weather-forecast/326966?day=2&lang=en-us"
    },
    {
      Date: "2020-01-08T07:00:00+00:00",
      EpochDate: 1578466800,
      Temperature: {
        Minimum: {
          Value: 40,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 50,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 6,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Night: {
        Icon: 18,
        IconPhrase: "Rain",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Moderate"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/birmingham/b5-5/daily-weather-forecast/326966?day=3&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/birmingham/b5-5/daily-weather-forecast/326966?day=3&lang=en-us"
    },
    {
      Date: "2020-01-09T07:00:00+00:00",
      EpochDate: 1578553200,
      Temperature: {
        Minimum: {
          Value: 36,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 44,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 38,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/birmingham/b5-5/daily-weather-forecast/326966?day=4&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/birmingham/b5-5/daily-weather-forecast/326966?day=4&lang=en-us"
    },
    {
      Date: "2020-01-10T07:00:00+00:00",
      EpochDate: 1578639600,
      Temperature: {
        Minimum: {
          Value: 40,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 43,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 6,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Night: {
        Icon: 38,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/birmingham/b5-5/daily-weather-forecast/326966?day=5&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/birmingham/b5-5/daily-weather-forecast/326966?day=5&lang=en-us"
    }
  ]
};

//London weather static data
const weatherLondonStaticData = {
  Location: "London",
  Headline: {
    EffectiveDate: "2020-01-06T16:00:00+00:00",
    EffectiveEpochDate: 1578326400,
    Severity: 5,
    Text: "Expect showers late Monday afternoon",
    Category: "rain",
    EndDate: "2020-01-06T22:00:00+00:00",
    EndEpochDate: 1578348000,
    MobileLink:
      "http://m.accuweather.com/en/gb/london/ec4a-2/extended-weather-forecast/328328?lang=en-us",
    Link:
      "http://www.accuweather.com/en/gb/london/ec4a-2/daily-weather-forecast/328328?lang=en-us"
  },
  DailyForecasts: [
    {
      Date: "2020-01-06T07:00:00+00:00",
      EpochDate: 1578294000,
      Temperature: {
        Minimum: {
          Value: 40,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 48,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 36,
        IconPhrase: "Intermittent clouds",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/london/ec4a-2/daily-weather-forecast/328328?day=1&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/london/ec4a-2/daily-weather-forecast/328328?day=1&lang=en-us"
    },
    {
      Date: "2020-01-07T07:00:00+00:00",
      EpochDate: 1578380400,
      Temperature: {
        Minimum: {
          Value: 51,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 54,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 13,
        IconPhrase: "Mostly cloudy w/ showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/london/ec4a-2/daily-weather-forecast/328328?day=2&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/london/ec4a-2/daily-weather-forecast/328328?day=2&lang=en-us"
    },
    {
      Date: "2020-01-08T07:00:00+00:00",
      EpochDate: 1578466800,
      Temperature: {
        Minimum: {
          Value: 49,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 53,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 6,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Night: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/london/ec4a-2/daily-weather-forecast/328328?day=3&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/london/ec4a-2/daily-weather-forecast/328328?day=3&lang=en-us"
    },
    {
      Date: "2020-01-09T07:00:00+00:00",
      EpochDate: 1578553200,
      Temperature: {
        Minimum: {
          Value: 40,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 51,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 38,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/london/ec4a-2/daily-weather-forecast/328328?day=4&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/london/ec4a-2/daily-weather-forecast/328328?day=4&lang=en-us"
    },
    {
      Date: "2020-01-10T07:00:00+00:00",
      EpochDate: 1578639600,
      Temperature: {
        Minimum: {
          Value: 42,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 46,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 6,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Night: {
        Icon: 8,
        IconPhrase: "Dreary",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/london/ec4a-2/daily-weather-forecast/328328?day=5&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/london/ec4a-2/daily-weather-forecast/328328?day=5&lang=en-us"
    }
  ]
};

//Cardiff weather static data
const weatherCardiffStaticData = {
  Location: "Cardiff",
  Headline: {
    EffectiveDate: "2020-01-09T01:00:00+00:00",
    EffectiveEpochDate: 1578531600,
    Severity: 3,
    Text:
      "Expect showery weather late Wednesday night through Thursday evening",
    Category: "rain",
    EndDate: "2020-01-10T01:00:00+00:00",
    EndEpochDate: 1578618000,
    MobileLink:
      "http://m.accuweather.com/en/gb/cardiff/cf10-3/extended-weather-forecast/327202?lang=en-us",
    Link:
      "http://www.accuweather.com/en/gb/cardiff/cf10-3/daily-weather-forecast/327202?lang=en-us"
  },
  DailyForecasts: [
    {
      Date: "2020-01-06T07:00:00+00:00",
      EpochDate: 1578294000,
      Temperature: {
        Minimum: {
          Value: 43,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 50,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 18,
        IconPhrase: "Rain",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 38,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/cardiff/cf10-3/daily-weather-forecast/327202?day=1&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/cardiff/cf10-3/daily-weather-forecast/327202?day=1&lang=en-us"
    },
    {
      Date: "2020-01-07T07:00:00+00:00",
      EpochDate: 1578380400,
      Temperature: {
        Minimum: {
          Value: 46,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 54,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/cardiff/cf10-3/daily-weather-forecast/327202?day=2&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/cardiff/cf10-3/daily-weather-forecast/327202?day=2&lang=en-us"
    },
    {
      Date: "2020-01-08T07:00:00+00:00",
      EpochDate: 1578466800,
      Temperature: {
        Minimum: {
          Value: 42,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 50,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Moderate"
      },
      Night: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Moderate"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/cardiff/cf10-3/daily-weather-forecast/327202?day=3&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/cardiff/cf10-3/daily-weather-forecast/327202?day=3&lang=en-us"
    },
    {
      Date: "2020-01-09T07:00:00+00:00",
      EpochDate: 1578553200,
      Temperature: {
        Minimum: {
          Value: 36,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 46,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 40,
        IconPhrase: "Mostly cloudy w/ showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/cardiff/cf10-3/daily-weather-forecast/327202?day=4&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/cardiff/cf10-3/daily-weather-forecast/327202?day=4&lang=en-us"
    },
    {
      Date: "2020-01-10T07:00:00+00:00",
      EpochDate: 1578639600,
      Temperature: {
        Minimum: {
          Value: 42,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 44,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 6,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Night: {
        Icon: 38,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/cardiff/cf10-3/daily-weather-forecast/327202?day=5&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/cardiff/cf10-3/daily-weather-forecast/327202?day=5&lang=en-us"
    }
  ]
};

// Manchester weather static data
const weatherManchesterStaticData = {
  Location: "Manchester",
  Headline: {
    EffectiveDate: "2020-01-08T19:00:00+00:00",
    EffectiveEpochDate: 1578510000,
    Severity: 4,
    Text: "Rain Wednesday evening",
    Category: "rain",
    EndDate: "2020-01-09T01:00:00+00:00",
    EndEpochDate: 1578531600,
    MobileLink:
      "http://m.accuweather.com/en/gb/manchester/m15-6/extended-weather-forecast/329260?lang=en-us",
    Link:
      "http://www.accuweather.com/en/gb/manchester/m15-6/daily-weather-forecast/329260?lang=en-us"
  },
  DailyForecasts: [
    {
      Date: "2020-01-06T07:00:00+00:00",
      EpochDate: 1578294000,
      Temperature: {
        Minimum: {
          Value: 40,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 46,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 36,
        IconPhrase: "Intermittent clouds",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/manchester/m15-6/daily-weather-forecast/329260?day=1&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/manchester/m15-6/daily-weather-forecast/329260?day=1&lang=en-us"
    },
    {
      Date: "2020-01-07T07:00:00+00:00",
      EpochDate: 1578380400,
      Temperature: {
        Minimum: {
          Value: 43,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 55,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/manchester/m15-6/daily-weather-forecast/329260?day=2&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/manchester/m15-6/daily-weather-forecast/329260?day=2&lang=en-us"
    },
    {
      Date: "2020-01-08T07:00:00+00:00",
      EpochDate: 1578466800,
      Temperature: {
        Minimum: {
          Value: 37,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 46,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 7,
        IconPhrase: "Cloudy",
        HasPrecipitation: false
      },
      Night: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Moderate"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/manchester/m15-6/daily-weather-forecast/329260?day=3&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/manchester/m15-6/daily-weather-forecast/329260?day=3&lang=en-us"
    },
    {
      Date: "2020-01-09T07:00:00+00:00",
      EpochDate: 1578553200,
      Temperature: {
        Minimum: {
          Value: 34,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 43,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 38,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/manchester/m15-6/daily-weather-forecast/329260?day=4&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/manchester/m15-6/daily-weather-forecast/329260?day=4&lang=en-us"
    },
    {
      Date: "2020-01-10T07:00:00+00:00",
      EpochDate: 1578639600,
      Temperature: {
        Minimum: {
          Value: 39,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 42,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 6,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Night: {
        Icon: 38,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/manchester/m15-6/daily-weather-forecast/329260?day=5&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/manchester/m15-6/daily-weather-forecast/329260?day=5&lang=en-us"
    }
  ]
};

const weatherNewcastleStaticData = {
  Location: "Newcastle",
  Headline: {
    EffectiveDate: "2020-01-10T07:00:00+02:00",
    EffectiveEpochDate: 1578632400,
    Severity: 4,
    Text: "Noticeably cooler Friday",
    Category: "cooler",
    EndDate: "2020-01-10T19:00:00+02:00",
    EndEpochDate: 1578675600,
    MobileLink:
      "http://m.accuweather.com/en/za/newcastle/298885/extended-weather-forecast/298885?lang=en-us",
    Link:
      "http://www.accuweather.com/en/za/newcastle/298885/daily-weather-forecast/298885?lang=en-us"
  },
  DailyForecasts: [
    {
      Date: "2020-01-08T07:00:00+02:00",
      EpochDate: 1578459600,
      Temperature: {
        Minimum: {
          Value: 61,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 87,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 6,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Night: {
        Icon: 7,
        IconPhrase: "Cloudy",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/za/newcastle/298885/daily-weather-forecast/298885?day=1&lang=en-us",
      Link:
        "http://www.accuweather.com/en/za/newcastle/298885/daily-weather-forecast/298885?day=1&lang=en-us"
    },
    {
      Date: "2020-01-09T07:00:00+02:00",
      EpochDate: 1578546000,
      Temperature: {
        Minimum: {
          Value: 64,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 88,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 16,
        IconPhrase: "Mostly cloudy w/ t-storms",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 15,
        IconPhrase: "Thunderstorms",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Moderate"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/za/newcastle/298885/daily-weather-forecast/298885?day=2&lang=en-us",
      Link:
        "http://www.accuweather.com/en/za/newcastle/298885/daily-weather-forecast/298885?day=2&lang=en-us"
    },
    {
      Date: "2020-01-10T07:00:00+02:00",
      EpochDate: 1578632400,
      Temperature: {
        Minimum: {
          Value: 57,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 68,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 8,
        IconPhrase: "Dreary",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/za/newcastle/298885/daily-weather-forecast/298885?day=3&lang=en-us",
      Link:
        "http://www.accuweather.com/en/za/newcastle/298885/daily-weather-forecast/298885?day=3&lang=en-us"
    },
    {
      Date: "2020-01-11T07:00:00+02:00",
      EpochDate: 1578718800,
      Temperature: {
        Minimum: {
          Value: 61,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 80,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 8,
        IconPhrase: "Dreary",
        HasPrecipitation: false
      },
      Night: {
        Icon: 35,
        IconPhrase: "Partly cloudy",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/za/newcastle/298885/daily-weather-forecast/298885?day=4&lang=en-us",
      Link:
        "http://www.accuweather.com/en/za/newcastle/298885/daily-weather-forecast/298885?day=4&lang=en-us"
    },
    {
      Date: "2020-01-12T07:00:00+02:00",
      EpochDate: 1578805200,
      Temperature: {
        Minimum: {
          Value: 62,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 85,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 6,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Heavy"
      },
      Night: {
        Icon: 7,
        IconPhrase: "Cloudy",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Heavy"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/za/newcastle/298885/daily-weather-forecast/298885?day=5&lang=en-us",
      Link:
        "http://www.accuweather.com/en/za/newcastle/298885/daily-weather-forecast/298885?day=5&lang=en-us"
    }
  ]
};

const weatherLiverpoolStaticData = {
  Location: "Liverpool",
  Headline: {
    EffectiveDate: "2020-01-08T19:00:00+00:00",
    EffectiveEpochDate: 1578510000,
    Severity: 3,
    Text: "Expect rainy weather Wednesday evening through Thursday afternoon",
    Category: "rain",
    EndDate: "2020-01-09T19:00:00+00:00",
    EndEpochDate: 1578596400,
    MobileLink:
      "http://m.accuweather.com/en/gb/liverpool/l7-9/extended-weather-forecast/330510?lang=en-us",
    Link:
      "http://www.accuweather.com/en/gb/liverpool/l7-9/daily-weather-forecast/330510?lang=en-us"
  },
  DailyForecasts: [
    {
      Date: "2020-01-08T07:00:00+00:00",
      EpochDate: 1578466800,
      Temperature: {
        Minimum: {
          Value: 42,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 50,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 4,
        IconPhrase: "Intermittent clouds",
        HasPrecipitation: false
      },
      Night: {
        Icon: 18,
        IconPhrase: "Rain",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/liverpool/l7-9/daily-weather-forecast/330510?day=1&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/liverpool/l7-9/daily-weather-forecast/330510?day=1&lang=en-us"
    },
    {
      Date: "2020-01-09T07:00:00+00:00",
      EpochDate: 1578553200,
      Temperature: {
        Minimum: {
          Value: 38,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 50,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 18,
        IconPhrase: "Rain",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 39,
        IconPhrase: "Partly cloudy w/ showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/liverpool/l7-9/daily-weather-forecast/330510?day=2&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/liverpool/l7-9/daily-weather-forecast/330510?day=2&lang=en-us"
    },
    {
      Date: "2020-01-10T07:00:00+00:00",
      EpochDate: 1578639600,
      Temperature: {
        Minimum: {
          Value: 42,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 47,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 4,
        IconPhrase: "Intermittent clouds",
        HasPrecipitation: false
      },
      Night: {
        Icon: 7,
        IconPhrase: "Cloudy",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/liverpool/l7-9/daily-weather-forecast/330510?day=3&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/liverpool/l7-9/daily-weather-forecast/330510?day=3&lang=en-us"
    },
    {
      Date: "2020-01-11T07:00:00+00:00",
      EpochDate: 1578726000,
      Temperature: {
        Minimum: {
          Value: 43,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 54,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/liverpool/l7-9/daily-weather-forecast/330510?day=4&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/liverpool/l7-9/daily-weather-forecast/330510?day=4&lang=en-us"
    },
    {
      Date: "2020-01-12T07:00:00+00:00",
      EpochDate: 1578812400,
      Temperature: {
        Minimum: {
          Value: 39,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 47,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 14,
        IconPhrase: "Partly sunny w/ showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 33,
        IconPhrase: "Clear",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/liverpool/l7-9/daily-weather-forecast/330510?day=5&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/liverpool/l7-9/daily-weather-forecast/330510?day=5&lang=en-us"
    }
  ]
};

const weatherGlasgowStaticData = {
  Location: "Glasgow",
  Headline: {
    EffectiveDate: "2020-01-11T01:00:00+00:00",
    EffectiveEpochDate: 1578704400,
    Severity: 4,
    Text: "Expect rainy weather late Friday night through Saturday afternoon",
    Category: "rain",
    EndDate: "2020-01-11T19:00:00+00:00",
    EndEpochDate: 1578769200,
    MobileLink:
      "http://m.accuweather.com/en/gb/glasgow/g3-8/extended-weather-forecast/328226?lang=en-us",
    Link:
      "http://www.accuweather.com/en/gb/glasgow/g3-8/daily-weather-forecast/328226?lang=en-us"
  },
  DailyForecasts: [
    {
      Date: "2020-01-08T07:00:00+00:00",
      EpochDate: 1578466800,
      Temperature: {
        Minimum: {
          Value: 36,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 42,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 7,
        IconPhrase: "Cloudy",
        HasPrecipitation: false
      },
      Night: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/glasgow/g3-8/daily-weather-forecast/328226?day=1&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/glasgow/g3-8/daily-weather-forecast/328226?day=1&lang=en-us"
    },
    {
      Date: "2020-01-09T07:00:00+00:00",
      EpochDate: 1578553200,
      Temperature: {
        Minimum: {
          Value: 32,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 41,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 36,
        IconPhrase: "Intermittent clouds",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/glasgow/g3-8/daily-weather-forecast/328226?day=2&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/glasgow/g3-8/daily-weather-forecast/328226?day=2&lang=en-us"
    },
    {
      Date: "2020-01-10T07:00:00+00:00",
      EpochDate: 1578639600,
      Temperature: {
        Minimum: {
          Value: 41,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 44,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 7,
        IconPhrase: "Cloudy",
        HasPrecipitation: false
      },
      Night: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/glasgow/g3-8/daily-weather-forecast/328226?day=3&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/glasgow/g3-8/daily-weather-forecast/328226?day=3&lang=en-us"
    },
    {
      Date: "2020-01-11T07:00:00+00:00",
      EpochDate: 1578726000,
      Temperature: {
        Minimum: {
          Value: 37,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 54,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 18,
        IconPhrase: "Rain",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Moderate"
      },
      Night: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/glasgow/g3-8/daily-weather-forecast/328226?day=4&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/glasgow/g3-8/daily-weather-forecast/328226?day=4&lang=en-us"
    },
    {
      Date: "2020-01-12T07:00:00+00:00",
      EpochDate: 1578812400,
      Temperature: {
        Minimum: {
          Value: 38,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 42,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 13,
        IconPhrase: "Mostly cloudy w/ showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 35,
        IconPhrase: "Partly cloudy",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/glasgow/g3-8/daily-weather-forecast/328226?day=5&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/glasgow/g3-8/daily-weather-forecast/328226?day=5&lang=en-us"
    }
  ]
};

const weatherEdinburghStaticData = {
  Location: "Edinburgh",
  Headline: {
    EffectiveDate: "2020-01-10T19:00:00+00:00",
    EffectiveEpochDate: 1578682800,
    Severity: 3,
    Text: "Expect rainy weather Friday evening through Saturday afternoon",
    Category: "rain",
    EndDate: "2020-01-11T19:00:00+00:00",
    EndEpochDate: 1578769200,
    MobileLink:
      "http://m.accuweather.com/en/gb/edinburgh/eh1-3/extended-weather-forecast/327336?lang=en-us",
    Link:
      "http://www.accuweather.com/en/gb/edinburgh/eh1-3/daily-weather-forecast/327336?lang=en-us"
  },
  DailyForecasts: [
    {
      Date: "2020-01-08T07:00:00+00:00",
      EpochDate: 1578466800,
      Temperature: {
        Minimum: {
          Value: 36,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 42,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 7,
        IconPhrase: "Cloudy",
        HasPrecipitation: false
      },
      Night: {
        Icon: 38,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/edinburgh/eh1-3/daily-weather-forecast/327336?day=1&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/edinburgh/eh1-3/daily-weather-forecast/327336?day=1&lang=en-us"
    },
    {
      Date: "2020-01-09T07:00:00+00:00",
      EpochDate: 1578553200,
      Temperature: {
        Minimum: {
          Value: 33,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 41,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Night: {
        Icon: 36,
        IconPhrase: "Intermittent clouds",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/edinburgh/eh1-3/daily-weather-forecast/327336?day=2&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/edinburgh/eh1-3/daily-weather-forecast/327336?day=2&lang=en-us"
    },
    {
      Date: "2020-01-10T07:00:00+00:00",
      EpochDate: 1578639600,
      Temperature: {
        Minimum: {
          Value: 41,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 45,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 4,
        IconPhrase: "Intermittent clouds",
        HasPrecipitation: false
      },
      Night: {
        Icon: 18,
        IconPhrase: "Rain",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/edinburgh/eh1-3/daily-weather-forecast/327336?day=3&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/edinburgh/eh1-3/daily-weather-forecast/327336?day=3&lang=en-us"
    },
    {
      Date: "2020-01-11T07:00:00+00:00",
      EpochDate: 1578726000,
      Temperature: {
        Minimum: {
          Value: 38,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 55,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 18,
        IconPhrase: "Rain",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Moderate"
      },
      Night: {
        Icon: 12,
        IconPhrase: "Showers",
        HasPrecipitation: true,
        PrecipitationType: "Rain",
        PrecipitationIntensity: "Light"
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/edinburgh/eh1-3/daily-weather-forecast/327336?day=4&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/edinburgh/eh1-3/daily-weather-forecast/327336?day=4&lang=en-us"
    },
    {
      Date: "2020-01-12T07:00:00+00:00",
      EpochDate: 1578812400,
      Temperature: {
        Minimum: {
          Value: 38,
          Unit: "F",
          UnitType: 18
        },
        Maximum: {
          Value: 43,
          Unit: "F",
          UnitType: 18
        }
      },
      Day: {
        Icon: 6,
        IconPhrase: "Mostly cloudy",
        HasPrecipitation: false
      },
      Night: {
        Icon: 34,
        IconPhrase: "Mostly clear",
        HasPrecipitation: false
      },
      Sources: ["AccuWeather"],
      MobileLink:
        "http://m.accuweather.com/en/gb/edinburgh/eh1-3/daily-weather-forecast/327336?day=5&lang=en-us",
      Link:
        "http://www.accuweather.com/en/gb/edinburgh/eh1-3/daily-weather-forecast/327336?day=5&lang=en-us"
    }
  ]
};

// store all weather static data in single object
export const weatherStaticDataNationwide = [];
weatherStaticDataNationwide.push(weatherBirminghamStaticData);
weatherStaticDataNationwide.push(weatherLondonStaticData);
weatherStaticDataNationwide.push(weatherCardiffStaticData);
weatherStaticDataNationwide.push(weatherManchesterStaticData);
weatherStaticDataNationwide.push(weatherNewcastleStaticData);
weatherStaticDataNationwide.push(weatherLiverpoolStaticData);
weatherStaticDataNationwide.push(weatherGlasgowStaticData);
weatherStaticDataNationwide.push(weatherEdinburghStaticData);
