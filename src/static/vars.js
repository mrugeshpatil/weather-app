export const API =
  "http://dataservice.accuweather.com/forecasts/v1/daily/5day/";
export const APIKEY = "xgouwExJpIVyBcDkhCaneKtFLJbfIcoZ";

// all locations specifies associated with id
// the accuwetaher API only pull individual forcast based on location ID
export const location = [
  {
    name: "Birmingham",
    id: 326966
  },
  {
    name: "London",
    id: 328328
  },
  {
    name: "Cardiff",
    id: 327202
  },
  {
    name: "Manchester",
    id: 329260
  },
  {
    name: "Newcastle",
    id: 298885
  },
  {
    name: "Liverpool",
    id: 330510
  },
  {
    name: "Glasgow",
    id: 328226
  },
  {
    name: "Edinburgh",
    id: 327336
  }
];
